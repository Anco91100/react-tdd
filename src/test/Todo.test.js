import { shallow, configure } from "enzyme";
import React from "react";
import should from "should";
import Todo from "../components/Todo";
import TodoList from "../components/TodoList"
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

configure({ adapter: new Adapter() });

describe('Todo component',() => {
    it('render the TodoList', () => {
        const wrapper = shallow(<Todo/>)
        should(wrapper.find(TodoList)).have.lengthOf(1);
    });
});