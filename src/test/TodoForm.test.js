import { shallow, configure, mount } from "enzyme";
import {render, screen, fireEvent} from '@testing-library/react'
import sinon from 'sinon';
import React from "react";
import should from "should";
import TodoForm from "../components/TodoForm"
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import {store} from '../redux/store';
import { Provider } from "react-redux";

configure({ adapter: new Adapter() });

describe('TodoForm Component', () => {
    it('render form and submit', () => {
        const onSubmit = sinon.spy();
        const wrapper = mount(<Provider store={store}><TodoForm onSubmit={onSubmit}/></Provider>);

        const form = wrapper.find('form');
        form.simulate('submit');
        should(onSubmit.calledOnce).be.true;
    });
    it('render input', () => {
        const onSubmit = sinon.spy();
        const wrapper = mount(<Provider store={store}><TodoForm onSubmit={onSubmit}/></Provider>);

        const input = wrapper.find('input');
        should(input).have.lengthOf(1);
    });
});

test('Form test', () => {
    const onSubmit = sinon.spy();
    const wrapper = render(<Provider store={store}><TodoForm onSubmit={onSubmit}/></Provider>);
    const input = screen.getByTestId('input-form');
    fireEvent.change(input, { target: { value: 'Salut'}})
    expect(input.value).toBe('Salut');
});
