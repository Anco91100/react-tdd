import React from 'react';

export default class ForbiddenText extends React.Component {
    render(){
        return(
            <div>
            {this.props.forbidden && <h4 style={{color:'red'}}> Ajoutez un text pour l'enregistrer.</h4>}
            </div>
        )
    }
}