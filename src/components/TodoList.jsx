import React from 'react';
import { connect } from 'react-redux';
import TodoItem from './TodoItem';



class TodoList extends React.Component {
    render(){
        return (
            <div>
                <h2>Task in progress</h2>
                { this.props.todo && Array.isArray(this.props.todo) && this.props.todo.map((task)=>{
                    return (!task.checked && (<TodoItem
                    task={task}
                    key={task.id}
                    />))
                })}
                <h2>Completed task</h2>
                { this.props.todo && Array.isArray(this.props.todo) && this.props.todo.map((task)=>{
                    return (task.checked && (<TodoItem
                    task={task}
                    key={task.id}
                    />))
                })}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        todo: state.todo
    }
}

export default connect(mapStateToProps, null)(TodoList);