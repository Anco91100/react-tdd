import React from 'react';
import { connect } from 'react-redux';
import {addTask} from '../redux/todo/todoSlice'
import ForbiddenText from './ForbiddenText';

class TodoForm extends React.Component {

    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            text: ''
        }
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if(this.state.text.length >0 && this.state.text.match("\\S+")){
        this.props.addTask({text: this.state.text})
        this.setState({text: '',forbidden: false})
        }
        else {
            this.setState({forbidden: true});
        }
    }

    handleChange = (event) => {
        this.setState({text: event.target.value})
    }

    render(){
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <input
                    type="text"
                    placeholder="Ajouter un text"
                    value={this.state.text}
                    onChange={this.handleChange}
                    data-testid="input-form"
                    />
                </form>
                <ForbiddenText forbidden={this.state.forbidden} />
            </div>
        )
    }

}


const mapDispatchToProps = (dispatch)=> {
return {
    addTask: (text) => dispatch(addTask(text))
}
};

export default connect(null, mapDispatchToProps)(TodoForm);