import React from 'react';
import {connect} from 'react-redux';
import {deleteTask, checkedTask, updateTodo} from '../redux/todo/todoSlice';

class TodoItem extends React.Component {
    constructor(props){
        super(props);
        this.onDelete = this.onDelete.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.onUpdate = this.onUpdate.bind(this);

        this.state = {
            editing: false,
            text: ''
        }
    }

    onDelete = (id) => {
        this.props.deleteTask({id: id})
    }
    
    onUpdate = () => {
        this.setState({editing: true});
    }

    handleChange = (id) => {
        this.props.checkedTask({id:id});
    }

    onEditing = (e) => {
        this.setState({text: e.target.value})
    }

    onSubmit = (e, id) => {
        e.preventDefault();
        this.props.updateTodo({text: this.state.text, id:id});
        this.setState({text: '', editing: false});
    }



    render(){
        console.log(this.props.task.checked);
        return (
            <div>
                <label>
                    <input
                    type="checkbox"
                    checked={this.props.task.checked}
                    onChange={() => this.handleChange(this.props.task.id)}
                    />
                    {this.props.task.text}
                    <span
                    onClick={() => this.onDelete(this.props.task.id)}
                    role="button"
                    style={{ padding: "5px", marginLeft: "20px",cursor:"pointer" }}
                    >
                        x
                    </span>
                </label>
                <span
                    onClick={this.onUpdate}
                    role="button"
                    style={{ padding: "5px", marginLeft: "20px", cursor:"pointer" }}
                    >
                        O
                    </span>
                    { this.state.editing && 
                    <form onSubmit={(event) => this.onSubmit(event, this.props.task.id)}>
                        <input
                        type="text"
                        placeholder="Ajouter un text"
                        value={this.state.text}
                        onChange={(event) => this.onEditing(event)}
                        />
                    </form>
                    }
            </div>
        )
    }
}


const mapDispatchToProps = {deleteTask, checkedTask, updateTodo};

export default connect(null, mapDispatchToProps)(TodoItem);
