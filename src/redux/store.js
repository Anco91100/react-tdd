import { configureStore } from '@reduxjs/toolkit'
import todoSliceReducer from './todo/todoSlice'
export const store = configureStore({
  reducer: {
    todo: todoSliceReducer,
  },
});
