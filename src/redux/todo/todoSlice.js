import { createSlice } from "@reduxjs/toolkit";

export const todoSlice = createSlice({
    name:'todo',
    initialState:[],
    reducers:{
        addTask: (state, action) => {
            const newTask = {
                id: Date.now(),
                text: action.payload.text,
                checked: false
            }
            state.push(newTask);
        },
        deleteTask: (state, action) => state.filter((t) => t.id !== action.payload.id),
        checkedTask: (state, action) => {
            return state.map( (t) => {
                if(t.id === action.payload.id){
                    return {
                        ...t,
                        checked: !t.checked
                    }
                }
                return t;
            })
        },
        updateTodo: (state, action) => {
            return state.map((t) => {
                if(t.id === action.payload.id){
                    return {
                        ...t,
                        text: action.payload.text
                    }
                }
                return t;
            })
        }
    }
})

export const {addTask, deleteTask, checkedTask, updateTodo} = todoSlice.actions;

export default todoSlice.reducer;