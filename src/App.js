import logo from './logo.svg';
import './App.css';
import Todo from './components/Todo';
import TodoForm from './components/TodoForm';

function App() {
  return (
    <div>
      <TodoForm/>
      <Todo/>
    </div>
    );
}

export default App;
